//
//  MovieDetailViewController.swift
//  Movies
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import RealmSwift

class MovieDetailViewController: BaseViewController {
    
    @IBOutlet var tableview: UITableView!
    @IBOutlet var thumb: UIImageView!
    @IBOutlet var lblName: UILabel!
    var movie: MovieResults!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableview.estimatedRowHeight = 70
        self.tableview.reloadData()
        
        let url = String(format: "http://image.tmdb.org/t/p/w185/%@", self.movie.posterPath!)
        self.thumb?.sd_setImage(with: URL(string: url), completed: nil)
        self.lblName.text = self.movie.title
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btFavorite(_ sender: UIButton) {
        
        // add favorite
        
        let realm = try! Realm()
        
        try! realm.write {
            
            let favoriteMovie = FavoriteMovie()
            favoriteMovie.title = self.movie.title!
            favoriteMovie.posterPath = self.movie.posterPath!
            favoriteMovie.overview = self.movie.overview!
            realm.add(favoriteMovie)
            
            self.showAlertView("Filme adicionado em favoritos!")
        }
    }
}

extension MovieDetailViewController: UITableViewDataSource{
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    private func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.01
    }
    
    private func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .orange
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = self.movie.overview

        return cell
    }
}
