//
//  FavoriteViewController.swift
//  Movies
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import RealmSwift

class FavoriteViewController: UIViewController {
    
    @IBOutlet var tableview: UITableView!
    var favorites = try! Realm().objects(FavoriteMovie.self)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableview.register(UINib(nibName: "FavoriteCell", bundle: nil), forCellReuseIdentifier: "favoriteCell")
        self.tableview.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeFavorite(sender: AnyObject) {
        
        let bt = sender as! UIButton
        let object = self.favorites[bt.tag]
        
        let realm = try! Realm()
        try! realm.write {
            realm.delete(object)
        }
        self.tableview.deleteRows(at: [IndexPath(row: bt.tag, section: 0)], with: .fade)
        
        self.favorites = try! Realm().objects(FavoriteMovie.self)
        if self.favorites.count == 0 {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}

extension FavoriteViewController: UITableViewDataSource{
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 127
    }
    
    private func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.01
    }
    
    private func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.favorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteCell", for: indexPath) as! FavoriteCell
        let favorite = self.favorites[indexPath.row]
        cell.configureCell(favorite: favorite)
        cell.btRemove.removeTarget(self, action: #selector(FavoriteViewController.removeFavorite(sender:)), for: .touchUpInside)
        cell.btRemove.addTarget(self, action: #selector(FavoriteViewController.removeFavorite(sender:)), for: .touchUpInside)
        cell.btRemove.tag = indexPath.row
        return cell
    }
}
