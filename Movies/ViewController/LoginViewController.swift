//
//  LoginViewController.swift
//  Movies
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Entrar"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btEnter(_ sender: UIButton) {
        
        if self.txtEmail.text?.characters.count == 0 || self.txtPassword.text?.characters.count == 0 {
            
            self.showAlertView("Todos os campos são obrigatórios")
        
        }else if self.isValidEmail(testStr: self.txtEmail.text!) == false {
            
            self.showAlertView("E-mail inválido")
            
        }else{
            
            UserInfo.setUserEmail(userEmail: self.txtEmail.text!)
            
            let moviesVC: MoviesViewController = self.storyboard?.instantiateViewController(withIdentifier: "MoviesViewController") as! MoviesViewController
            self.navigationController?.pushViewController(moviesVC, animated: true)
        }
    }
}
