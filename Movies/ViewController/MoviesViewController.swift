//
//  MoviesViewController.swift
//  Movies
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import RealmSwift

class MoviesViewController: BaseViewController {
    
    @IBOutlet var collectionview: UICollectionView?{
        didSet {
            collectionview?.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        }
    }
    @IBOutlet var actv: UIActivityIndicatorView!
    
    var movies = [MovieResults]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Filmes"
        self.requestMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addRightButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addRightButton() {
        
        let favorites = try! Realm().objects(FavoriteMovie.self)
        let rightButton = UIBarButtonItem(title: "Favoritos", style: .plain, target: self, action: #selector(MoviesViewController.openAccount))
        self.navigationItem.rightBarButtonItem = favorites.count != 0 ? rightButton : nil
    }
    
    func openAccount() {
        
        let favoriteVC: FavoriteViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteViewController") as! FavoriteViewController
        self.navigationController?.pushViewController(favoriteVC, animated: true)
    }
    
    func requestMovies() {
        
        self.actv.startAnimating()
        
        DownloadManager.getMoviesList { (moviesBase, code, success) in
            
            if let base = moviesBase {
                
                if base.results?.count != 0 {
                    self.movies = base.results!
                    self.collectionview?.reloadData()
                }else{
                    self.showAlertView("Não foi possível carregar a lista de filmes")
                }
                
            }else{
                
                self.showAlertView("Não foi possível carregar a lista de filmes")
            }
            
            self.actv.stopAnimating()
        }
    }
}

// MARK: UICollectionView Delegate/DataSource

extension MoviesViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.movies.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MovieCell
        let movie = self.movies[indexPath.row]
        cell.configureCell(movie: movie)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize(width: (self.view.frame.size.width-30)/2, height: 220)
    }
}

extension MoviesViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let movie = self.movies[indexPath.row]
        let movieDetailVC: MovieDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        movieDetailVC.movie = movie
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}
