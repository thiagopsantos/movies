//
//  FavoriteCell.swift
//  Movies
//
//  Created by Thiago Santos on 13/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class FavoriteCell: UITableViewCell {
    
    @IBOutlet var thumb: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btRemove: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(favorite: FavoriteMovie) {
        
        let url = String(format: "http://image.tmdb.org/t/p/w185/%@", favorite.posterPath)
        self.thumb.sd_setImage(with: URL(string: url), completed: nil)
        self.lblName.text = favorite.title
    }
}
