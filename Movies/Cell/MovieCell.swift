//
//  MovieCell.swift
//  Movies
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet var thumb: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(movie: MovieResults) {
        
        let url = String(format: "http://image.tmdb.org/t/p/w185/%@", movie.posterPath!)
        self.thumb?.sd_setImage(with: URL(string: url), completed: nil)
    }
}
