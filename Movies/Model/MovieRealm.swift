//
//  MovieRealm.swift
//  Movies
//
//  Created by Thiago Santos on 13/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import RealmSwift

class FavoriteMovie: Object {
    
    dynamic var title = ""
    dynamic var posterPath = ""
    dynamic var overview = ""
}
