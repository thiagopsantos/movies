//
//  UserInfo.swift
//  Post2b
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SAMKeychain

let KACCOUNT = Bundle.main.bundleIdentifier!
let KServiceUserEmail = "user_email"

class UserInfo: NSObject {
    
    // MARK: User - Data
    
    class func getEmail() -> String  {
        
        if let userEmail = SAMKeychain.password(forService: KServiceUserEmail, account: KACCOUNT) {
            return userEmail
        }
        return ""
    }

    class func setUserEmail(userEmail: String) {

        SAMKeychain.setPassword(userEmail, forService: KServiceUserEmail, account: KACCOUNT)
    }
    
    class func userSaved() -> Bool {
        
        if let userEmail = SAMKeychain.password(forService: KServiceUserEmail, account: KACCOUNT) {
            print(String(format: "userEmail: %@", userEmail))
            return true
        }
        return false
    }
    
    
    // MARK: User - Util
    
    class func logout() {

        SAMKeychain.deletePassword(forService: KServiceUserEmail, account: KACCOUNT)
    }
}
