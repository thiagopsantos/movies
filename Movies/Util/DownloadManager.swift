//
//  DownloadManager.swift
//  Post2b
//
//  Created by Thiago Santos on 12/01/17.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

let URL_BASE_MASTER = "https://api.themoviedb.org/3/movie/popular?api_key=faf9b59fe2191bb669cd11a5f1f460c7"

extension Request {
    public func debugLog() -> Self {
        debugPrint(self)
        return self
    }
}

class DownloadManager: NSObject {
    
    // MARK: - Methods - Helper
    
    class func GET(_ url: String, callback: @escaping (_ result: AnyObject?, _ code: Int) -> Void) {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .debugLog()
            .responseJSON { response in
//            print(response.request!)  // original URL request
//            print(response.response) // URL response
//            print(response.data)     // server data
//            print(response.result)   // result of response serialization
            
            if response.response!.statusCode == 200 {
                
                let JSON = response.result.value
                callback(JSON as AnyObject?, response.response!.statusCode)
                
            }else{
                
                callback(nil, response.response!.statusCode)
            }
        }
    }
    
    // MARK: - Methods - User
    
    class func getMoviesList( callback: @escaping (_ base: MovieBaseClass?, _ code: Int, _ success: Bool) -> Void) {
        
        DownloadManager.GET(URL_BASE_MASTER) { (result, code) in
            //print(result)
            
            if let JSON = result {
                
                let base = MovieBaseClass(object: JSON)
                callback(base, code, true)
                
            }else{
                
                callback(nil, code, false)
            }
        }
    }
}

