//
//  BaseViewController.swift
//  Movies
//
//  Created by Thiago Santos on 13/01/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = .orange
        
        let myBackButton = UIBarButtonItem.init(title: "Back", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = myBackButton
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black]
    }

    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: Methods - AlertView
    
    func showAlertView(_ message: String) {
        
        let alert = UIAlertController(title: "Movies", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        AppDelegate.appDelegate().window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    
    func showAlertView(_ title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        AppDelegate.appDelegate().window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    
    func showAlertView(_ title: String, message: String, cancelTitle: String, defaultTitle: String) {
        
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: defaultTitle, style: .default, handler: { action in
            self.didSelectAlertView(1)
        }))
        
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: { action in
            self.didSelectAlertView(0)
        }))
        AppDelegate.appDelegate().window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    
    func didSelectAlertView(_ index: NSNumber) {
        
        print(index)
    }

}
